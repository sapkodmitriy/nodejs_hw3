import express from 'express';
import {
  checkDuplicateEmail,
  checkValidSignUp,
  checkValidLogin,
} from '../middleware/validateAuthParams.js';
import {signup, signin} from '../controllers/auth.controller.js';

const router = new express.Router();

router.post('/register', [checkValidSignUp, checkDuplicateEmail], signup);
router.post('/login', [checkValidLogin], signin);

export default router;
