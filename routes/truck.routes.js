import express from 'express';
import verifyToken from '../middleware/authJwt.js';
import {
  allTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} from '../controllers/truck.controller.js';
import {checkRoleDriver} from '../middleware/validateUserRole.js';
import {
  checkTruckId,
  checkTruckType,
} from '../middleware/validateTruckParams.js';

const router = new express.Router();

router.get('/', [verifyToken, checkRoleDriver], allTrucks);
router.post('/', [verifyToken, checkRoleDriver, checkTruckType], addTruck);
router.get('/:id', [verifyToken, checkRoleDriver, checkTruckId], getTruck);
router.put(
    '/:id',
    [verifyToken, checkRoleDriver, checkTruckId, checkTruckType],
    updateTruck,
);
router.delete(
    '/:id',
    [verifyToken, checkRoleDriver, checkTruckId],
    deleteTruck,
);
router.post(
    '/:id/assign',
    [verifyToken, checkRoleDriver, checkTruckId],
    assignTruck,
);

export default router;
