/* eslint-disable require-jsdoc, camelcase */
import userSchema from '../validation/userSchema.js';
import User from '../models/user.model.js';
import { StatusCodes } from 'http-status-codes';
import validate from '../validation/validator.js';
import bcrypt from 'bcryptjs';

export function checkValidSignUp(req, res, next) {
  const { email, password, role } = req.body;
  if (!email || !password || !role) {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: `Not enough parameters. Must have 'email', 'password', 'role'`
    });
  }
  validate({ email, password, role }, userSchema, res, next);
}

export function checkValidLogin(req, res, next) {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: "Not enough parameters. Must have 'email', 'password'"
    });
  }
  validate({ email, password }, userSchema, res, next);
}

export async function checkValidNewPass(req, res, next) {
  const user = req.user;
  const { newPassword, oldPassword } = req.body;

  if (!newPassword || !oldPassword) {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: `Not enough parameters. Must have 'newPassword', 'oldPassword'`
    });
  }

  const passwordIsValid = await bcrypt.compare(oldPassword, user.password);
  if (!passwordIsValid) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ message: `'oldPassword' doesn't match` });
  }
  validate({ password: newPassword }, userSchema, res, next);
}

export function checkDuplicateEmail(req, res, next) {
  const { email } = req.body;

  // Check if user with this username exist
  User.getByEmail(email)
    .then((user) => {
      if (user) {
        return res.status(StatusCodes.BAD_REQUEST).json({
          message: 'User with this email already exist'
        });
      }

      next();
    })
    .catch(() => {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Internal server error'
      });
    });
}
