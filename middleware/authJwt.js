/* eslint-disable require-jsdoc, camelcase */
import jwt from 'jsonwebtoken';
import {StatusCodes} from 'http-status-codes';
import User from '../models/user.model.js';

const jwtSalt = process.env.JWTSECRET || 'SecretJWTSalt123!@$';
const JWS_REGEX = /^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/;

const getToken = (authorization) => {
  const subStringArray = authorization.split(' ');
  // Not empty string
  if (subStringArray.length > 0) {
    // Check if substring has jwt token format. If yes, return it
    for (const substring of subStringArray) {
      if (JWS_REGEX.test(substring)) {
        return substring;
      }
    }

    // Previous loop check may have problems with base64encoding
    // so check manualy if substring separated by '.' in three parts
    for (const substring of subStringArray) {
      if (/(\..*){2}/.test(substring)) {
        return substring;
      }
    }

    // Even if previous check doesn't work take longest
    // string because no other choice
    return subStringArray.sort((a, b) => b.length - a.length)[0];
  }

  return null;
};

export default function verifyToken(req, res, next) {
  const authorization = req.headers.authorization;
  if (!authorization) {
    return res.status(StatusCodes.BAD_REQUEST).send({
      message: 'Authorization header is empty',
    });
  }

  const token = getToken(authorization);

  if (!token) {
    return res.status(StatusCodes.BAD_REQUEST).send({
      message: 'Cannot find JWT token',
    });
  }

  jwt.verify(token, jwtSalt, async (err, decoded) => {
    if (err) {
      return res.status(StatusCodes.BAD_REQUEST).send({
        message: 'Invalid JWT token',
      });
    }

    const userId = decoded.id;
    try {
      const user = await User.getById(userId);
      if (!user) {
        return res.status(StatusCodes.BAD_REQUEST).send({
          message: 'Cannot find user with id: ' + userId,
        });
      }

      req.user = user;
      next();
    } catch (err) {
      res
          .status(StatusCodes.INTERNAL_SERVER_ERROR)
          .json({message: 'Internal server error'});
    }
  });
}
