/* eslint-disable require-jsdoc, camelcase */
import Truck from '../models/truck.model.js';
import {StatusCodes} from 'http-status-codes';

export async function allTrucks(req, res) {
  try {
    const trucks = await Truck.getAll();
    res.status(StatusCodes.OK).json({trucks});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}

export async function addTruck(req, res) {
  const user = req.user;
  const {type} = req.body;

  try {
    await Truck.insert(new Truck(user._id, type));

    res.status(StatusCodes.OK).json({message: 'Truck created successfully'});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}

export async function getTruck(req, res) {
  const {id} = req.params;

  try {
    const truck = await Truck.getById(id);

    if (!truck) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'No truck with this id'});
    }

    res.status(StatusCodes.OK).json({truck});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}

export async function updateTruck(req, res) {
  const user = req.user;
  const {id} = req.params;
  const {type} = req.body;

  try {
    const truck = await Truck.getById(id);
    if (!truck) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'No truck with this id'});
    }

    if (truck.assigned_to === user.id) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'Cannot change assigned to you truck'});
    }

    truck.type = type;
    await Truck.update(truck);

    res
        .status(StatusCodes.OK)
        .json({message: 'Truck details changed successfully'});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}

export async function deleteTruck(req, res) {
  const user = req.user;
  const {id} = req.params;

  try {
    const truck = await Truck.getById(id);
    if (!truck) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'No truck with this id'});
    }

    if (truck.assigned_to === user.id) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'Cannot delete assigned to you truck'});
    }

    if (truck.status === 'OL') {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'Cannot delete truck with active load'});
    }

    await Truck.delete(id);

    res.status(StatusCodes.OK).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}

export async function assignTruck(req, res) {
  const user = req.user;
  const {id} = req.params;

  try {
    const userTruck = await Truck.getByAssigned(user._id);
    if (userTruck) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'You already have assigned to you truck'});
    }

    const truck = await Truck.getById(id);
    if (!truck) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'No truck with this id'});
    }

    if (truck.assigned_to) {
      return res
          .status(StatusCodes.BAD_REQUEST)
          .json({message: 'This truck is already assigned to driver'});
    }

    truck.assigned_to = user.id;
    await Truck.update(truck);

    res.status(StatusCodes.OK).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .json({message: 'Internal server error'});
  }
}
