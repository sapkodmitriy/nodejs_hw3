/* eslint-disable require-jsdoc */
import {StatusCodes} from 'http-status-codes';

export default function validate(data, schema, res, next) {
  try {
    const {error} = schema.validate(data);
    if (error) {
      return res.status(StatusCodes.BAD_REQUEST).json({
        message: error.message,
      });
    }

    next();
  } catch (err) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      message: 'Internal server error',
    });
  }
}
