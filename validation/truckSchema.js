import Joi from 'joi';

export default Joi.object({
  id: Joi.string(),
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
});
