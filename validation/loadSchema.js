import Joi from 'joi';

const LIMIT_MAX = 50;

export default Joi.object({
  id: Joi.string(),
  name: Joi.string(),
  payload: Joi.number(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object({
    width: Joi.number(),
    length: Joi.number(),
    height: Joi.number(),
  }),
  status: Joi.string().valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
  limit: Joi.number().min(0).max(LIMIT_MAX),
  offset: Joi.number().min(0),
});
