export default Object.freeze({
  'SPRINTER': {dimensions: [300, 250, 170], carrying_capacity: 1700},
  'SMALL STRAIGHT': {dimensions: [500, 250, 170], carrying_capacity: 2500},
  'LARGE STRAIGHT': {dimensions: [700, 350, 200], carrying_capacity: 4000},
});
