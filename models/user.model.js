/* eslint-disable require-jsdoc */
import mongoose from 'mongoose';

const UserShema = new mongoose.Schema(
    {
      email: {type: String, required: true},
      password: {type: String, required: true},
      role: {type: String, required: true},
      createdDate: {type: String, required: true},
    },
    {versionKey: false},
);

const UserModel = mongoose.model('User', UserShema);

class User {
  constructor(email, password, role) {
    this.email = email;
    this.password = password;
    this.role = role;
    this.createdDate = new Date().toISOString();
  }

  static insert(user) {
    return new UserModel(user).save();
  }

  static getById(id) {
    return UserModel.findById({_id: id});
  }

  static getByEmail(email) {
    return UserModel.findOne({email: email});
  }

  static update(user) {
    return UserModel.findByIdAndUpdate(user._id, user);
  }

  static delete(id) {
    return UserModel.findByIdAndDelete(id);
  }
}

export default User;
